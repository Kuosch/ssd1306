/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stm32f0xx_hal.h"
#include "ssd1306.h"
#include "kuosch.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_I2C1_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_I2C1_Init();
  /* USER CODE BEGIN 2 */

  // Init display
  ssd1306_init();
  HAL_Delay(500);
  ssd1306_clearDisplay();
  HAL_Delay(200);
  ssd1306_displayImage(kuosch_logo);
//  ssd1306_displayImage(test_pattern_1);
  HAL_Delay(1000);

  ssd1306_displayRefresh();
  ssd1306_scrollVertLeft();


//  uint8_t data = 0;
//  uint16_t idx = 0;
//  for (int j = 0; j < DISPLAY_HEIGHT; j++) {
//	  for (int i = 0; i < DISPLAY_WIDTH/8; i++) {
//		  idx = j*(DISPLAY_WIDTH/8)+i;
//		  data = kuosch_logo[idx];
////		  data = 0xCC;
////		  ssd1306_setPixel(i*8+7, j, ((data & 0x80) >> 7));
////		  ssd1306_setPixel(i*8+6, j, ((data & 0x40) >> 6));
////		  ssd1306_setPixel(i*8+5, j, ((data & 0x20) >> 5));
////		  ssd1306_setPixel(i*8+4, j, ((data & 0x10) >> 4));
////		  ssd1306_setPixel(i*8+3, j, ((data & 0x08) >> 3));
////		  ssd1306_setPixel(i*8+2, j, ((data & 0x04) >> 2));
////		  ssd1306_setPixel(i*8+1, j, ((data & 0x02) >> 1));
////		  ssd1306_setPixel(i*8+0, j, ((data & 0x01) >> 0));
//
//		  ssd1306_setPixel(i*8+0, j, ((data & 0x80) >> 7));
//		  ssd1306_setPixel(i*8+1, j, ((data & 0x40) >> 6));
//		  ssd1306_setPixel(i*8+2, j, ((data & 0x20) >> 5));
//		  ssd1306_setPixel(i*8+3, j, ((data & 0x10) >> 4));
//		  ssd1306_setPixel(i*8+4, j, ((data & 0x08) >> 3));
//		  ssd1306_setPixel(i*8+5, j, ((data & 0x04) >> 2));
//		  ssd1306_setPixel(i*8+6, j, ((data & 0x02) >> 1));
//		  ssd1306_setPixel(i*8+7, j, ((data & 0x01) >> 0));
//
////		  ssd1306_setPixel(i*8+8, j+8, ((data & 0x01) >> 0));
////		  ssd1306_setPixel(i*8+9, j+8, ((data & 0x02) >> 1));
////		  ssd1306_setPixel(i*8+10, j+8, ((data & 0x04) >> 2));
////		  ssd1306_setPixel(i*8+11, j+8, ((data & 0x08) >> 3));
////		  ssd1306_setPixel(i*8+12, j+8, ((data & 0x10) >> 4));
////		  ssd1306_setPixel(i*8+13, j+8, ((data & 0x20) >> 5));
////		  ssd1306_setPixel(i*8+14, j+8, ((data & 0x40) >> 6));
////		  ssd1306_setPixel(i*8+15, j+8, ((data & 0x80) >> 7));
//	  }
//  }
  ssd1306_clearDisplay();
  ssd1306_displayRefresh();
  ssd1306_setCursor(0,0);

  ssd1306_displayRefresh();

  ssd1306_printString("12345", DejaVu_32, ON, 5);
  ssd1306_displayRefresh();

  char ch = '1';
  ssd1306_printChar(ch, DejaVu_32, ON, 0);
  ssd1306_displayRefresh();
  ssd1306_setCursor(24,0);
  ch = '2';
  ssd1306_printChar(ch, DejaVu_32, ON, 0);
  ssd1306_displayRefresh();
  ssd1306_setCursor(48,0);
  ch = '3';
  ssd1306_printChar(ch, DejaVu_32, ON, 0);
  ssd1306_displayRefresh();
  ssd1306_setCursor(72,0);
  ch = '4';
  ssd1306_printChar(ch, DejaVu_32, ON, 0);
  ssd1306_displayRefresh();
  ssd1306_setCursor(96,0);
  ch = '5';
  ssd1306_printChar(ch, DejaVu_32, ON, 0);
  ssd1306_displayRefresh();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
//  int xx = 0;
//  int yy = 0;
  char line1[] = {32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55};
  char line2[] = {56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79};
  char line3[] = {80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 101, 102, 103, 104};
  char line4[] = {105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128};
  while (1) {
//
////	  ssd1306_setCursor(xx,yy);
//	  ssd1306_setPixel(xx, yy, ON);
//	  xx += 5;
//	  yy += 7;
//	  xx %= 128;
//	  yy %= 64;
//	  ssd1306_displayRefresh();
//	  HAL_Delay(100);

//	  ssd1306_setCursor(0,0);
//
//	  ssd1306_printChar(ch, DejaVu_32, ON, 0);
//
//  	  if (ch > 126) {
//  		  ch = ' ';
//  		  ssd1306_displayImage(kuosch_logo);
//  		  ssd1306_displayRefresh();
//  		  HAL_Delay(700);
//  		  ssd1306_clearDisplay();
//  	  }
//  	  ch++;
//  	  ssd1306_displayRefresh();
//  	  HAL_Delay(100);

	  ssd1306_setCursor(4,0);
	  ssd1306_printString(line1, DejaVu_8, ON, 24);
	  ssd1306_displayRefresh();
	  ssd1306_setCursor(4,16);
	  ssd1306_printString(line2, DejaVu_8, ON, 24);
	  ssd1306_displayRefresh();
	  ssd1306_setCursor(4,32);
	  ssd1306_printString(line3, DejaVu_8, ON, 24);
	  ssd1306_displayRefresh();
	  ssd1306_setCursor(4,48);
	  ssd1306_printString(line4, DejaVu_8, ON, 24);
	  ssd1306_displayRefresh();



    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSI48;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI48;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x2000090E;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter 
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter 
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 38400;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LD2_Pin */
  GPIO_InitStruct.Pin = LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD2_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
