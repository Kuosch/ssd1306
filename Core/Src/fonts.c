#include "fonts.h"

#include "DejaVu_24x32.h"
#include "DejaVu_5x8.h"


FontType DejaVu_32 = {24, 32, DejaVu_24x32_bitmap};
FontType DejaVu_8  = {5, 8, DejaVu_5x8_bitmap};
