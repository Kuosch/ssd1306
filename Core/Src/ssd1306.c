/*
 * ssd1306.c
 *
 *  Created on: Mar 31, 2018
 *      Author: Kuosch
 */

#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "ssd1306.h"
#include "fonts.h"





extern I2C_HandleTypeDef SSD1306_I2C_PORT; // To kick out build error (defined in HAL)

struct {
	uint32_t displayInversion 	: 1;
} ssd1306_status;

static uint8_t buffer[(DISPLAY_WIDTH / 8) * DISPLAY_HEIGHT] = {
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};


void ssd1306_init(void)
{
	HAL_Delay(100);
	ssd1306_sendCommand(DISPLAY_OFF);
	ssd1306_sendCommand(SET_MEMORY_MODE);
	ssd1306_sendCommand(0x10); // Page addressing
	ssd1306_sendCommand(SET_PAGE_START_OFFS);
	ssd1306_sendCommand(COM_SCAN_DIR_DEC);
	ssd1306_sendCommand(SET_COLUMN_LOW_START); // Start addr 0
	ssd1306_sendCommand(SET_COLUMN_HIGH_START);
	ssd1306_sendCommand(SET_START_LINE);
	ssd1306_sendCommand(SET_CONTRAST);
	ssd1306_sendCommand(0xFF);	// Max contrast

//	ssd1306_sendCommand(SEGMENT_REMAP + 1);
	ssd1306_sendCommand(0xA1);// column address 127 is mapped to SEG0
	ssd1306_sendCommand(DISPLAY_NORMAL);
	ssd1306_sendCommand(MULTIPLEX_RATIO);
	ssd1306_sendCommand(0x3F);

	ssd1306_sendCommand(DISPLAY_ALL_RESUME);
	ssd1306_sendCommand(DISPLAY_OFFSET);
	ssd1306_sendCommand(0x00); // Offset zero

	ssd1306_sendCommand(SET_DISPLAY_CLOCK);
	ssd1306_sendCommand(0xF0); // Max freq.
	ssd1306_sendCommand(SET_PRE_CHARGE);
	ssd1306_sendCommand(0x22); // Value at reset

	ssd1306_sendCommand(COM_PINS);
	ssd1306_sendCommand(0x12); // Value at reset. Alt. config, no remap

	ssd1306_sendCommand(SET_VCOM_DESELECT_LVL);
	ssd1306_sendCommand(VCOM_LEVEL_077_VCC);

	ssd1306_sendCommand(CHARGE_PUMP);
	ssd1306_sendCommand(ENABLE_CHARGE_PUMP);
	ssd1306_sendCommand(DISPLAY_ON);

}

void ssd1306_sendCommand(uint8_t command)
{
//	uint8_t control_code = 0x00;
//	uint8_t i2c_buffer[2];
//	i2c_buffer[0] = control_code;
//	i2c_buffer[1] = command;
//	HAL_I2C_Master_Transmit(&SSD1306_I2C_PORT, SSD1306_I2C_ADDRESS, i2c_buffer, 2, I2C_TIMEOUT);
	HAL_I2C_Mem_Write(&SSD1306_I2C_PORT, SSD1306_I2C_ADDRESS, 0x00, 1, &command, 1, I2C_TIMEOUT);
}

void ssd1306_clearDisplay(void)
{
	memset(buffer, 0x00, DISPLAY_WIDTH * DISPLAY_HEIGHT / 8);
	ssd1306_displayRefresh();
}

void ssd1306_invertDisplay(void)
{
	if (ssd1306_status.displayInversion)
	{
		ssd1306_sendCommand(DISPLAY_NORMAL);
		ssd1306_status.displayInversion = 0;
	} else {
		ssd1306_sendCommand(DISPLAY_INVERSE);
		ssd1306_status.displayInversion = 1;
	}
}

void ssd1306_displayRefresh(void)
{
	/* Write the buffer to the screen*/
	for (int i=0; i < PAGES; i++) {
		ssd1306_sendCommand(SET_PAGE_START_OFFS + i); 	// Page i
		ssd1306_sendCommand(SET_COLUMN_LOW_START); 		// Column 0
		ssd1306_sendCommand(SET_COLUMN_HIGH_START); 	// Column 0
		/* Send 8 lines */
		HAL_I2C_Mem_Write(&SSD1306_I2C_PORT, SSD1306_I2C_ADDRESS, 0x40, 1, &buffer[i * DISPLAY_WIDTH], DISPLAY_WIDTH, 100);
	}
}

void ssd1306_setPixel(uint32_t x, uint32_t y, uint32_t type)
{
	switch (type) {
		case ON:
//			buffer[x + (y / 8) * DISPLAY_WIDTH] |= (1 << (y & 7));
			buffer[x + (y / 8) * DISPLAY_WIDTH] |= (1 << (y % 8));
			break;
		case OFF:
//			buffer[x + (y / 8) * DISPLAY_WIDTH] &= (1 << (y & 7));
			buffer[x + (y / 8) * DISPLAY_WIDTH] &= ~(1 << (y % 8));
			break;
		case INV:
//			buffer[x + (y / 8) * DISPLAY_WIDTH] ^= (1 << (y & 7));
			buffer[x + (y / 8) * DISPLAY_WIDTH] ^= (1 << (y % 8));
			break;
	}
}

void ssd1306_displayImage(uint8_t *image) {
	memcpy(buffer, image, DISPLAY_WIDTH * DISPLAY_HEIGHT / 8);
	ssd1306_displayRefresh();
}

void ssd1306_scrollRight(void)
{
	ssd1306_sendCommand(0x26); // Setup scroll
	ssd1306_sendCommand(0x00); // Dummy, 00h
	ssd1306_sendCommand(0x00); // Start page (0-7)
	ssd1306_sendCommand(0x06); // Scroll speed (0-7, not in order)
	ssd1306_sendCommand(0x07); // End page (7 for full screen)
	ssd1306_sendCommand(0x00); // Dummy, 00h
	ssd1306_sendCommand(0xFF); // End, FFh
	// Remember to call ssd1306_scrollStart()
}

void ssd1306_scrollLeft(void)
{
	ssd1306_sendCommand(0x27); // Setup scroll
	ssd1306_sendCommand(0x00); // Dummy, 00h
	ssd1306_sendCommand(0x00); // Start page (0-7)
	ssd1306_sendCommand(0x06); // Scroll speed (0-7, not in order)
	ssd1306_sendCommand(0x07); // End page (7 for full screen)
	ssd1306_sendCommand(0x00); // Dummy, 00h
	ssd1306_sendCommand(0xFF); // End, FFh
	// Remember to call ssd1306_scrollStart()
}

void ssd1306_scrollVertRight(void)
{
	ssd1306_sendCommand(0x29); // Setup scroll
	ssd1306_sendCommand(0x00); // Dummy, 00h
	ssd1306_sendCommand(0x00); // Start page (0-7)
	ssd1306_sendCommand(0x00); // Scroll speed (0-7, not in order)
	ssd1306_sendCommand(0x07); // End page (7 for full screen)
	ssd1306_sendCommand(0x02); // Scroll offset, 0-63 (00-3Fh)

	ssd1306_sendCommand(0xA3); // Vertical scroll area
	ssd1306_sendCommand(0x00); // Rows in top fixed area, 00-64 (00h-40h)
	ssd1306_sendCommand(0x40); // Rows to scroll, 00-64 (00h-40h)

	// Remember to call ssd1306_scrollStart()
}

void ssd1306_scrollVertLeft(void)
{
	ssd1306_sendCommand(0x2A); // Setup scroll
	ssd1306_sendCommand(0x00); // Dummy, 00h
	ssd1306_sendCommand(0x00); // Start page (0-7)
	ssd1306_sendCommand(0x00); // Scroll speed (0-7, not in order)
	ssd1306_sendCommand(0x07); // End page (7 for full screen)
	ssd1306_sendCommand(0x02); // Scroll offset, 0-63 (00-3Fh)
							   // Make even, small values scroll up, large down

	ssd1306_sendCommand(0xA3); // Vertical scroll area
	ssd1306_sendCommand(0x00); // Rows in top fixed area, 00-64 (00h-40h)
	ssd1306_sendCommand(0x40); // Rows to scroll, 00-64 (00h-40h)

	// Remember to call ssd1306_scrollStart()
}

void ssd1306_scrollStart(void)
{
	ssd1306_sendCommand(0x2F);
}

void ssd1306_scrollStop(void)
{
	ssd1306_sendCommand(0x2E);
}

char ssd1306_printString(char* string, FontType font, uint32_t type, uint32_t length) {
	char retval = 0;
	for (int i = 0; i < length; i++) {
		retval = ssd1306_printChar(string[i], font, type, 0);
		if (retval != string[i]) {
			// Print failed
			retval = 0;
			break;
		} else {
			ssd1306_cursor_x += font.width;
		}
	}
	if (retval != 0) {
		ssd1306_displayRefresh();
	}
	return retval;
}

/*
 * chr  : character to write to current cursor position
 * font : the font to use
 * type : colour of the character (on, off, or invert)
 * wrap : if there is not enough space on current line, continue from next line.
 */
char ssd1306_printChar(char chr, FontType font, uint32_t type, bool wrap) {
	char retval = chr;
	uint32_t i = 0;
	uint32_t j = 0;
	uint8_t byte = 0;

	// Check the font width in bytes
	uint8_t xblocks = font.width / 8;
	if (font.width % 8 > 0) {
		xblocks++;
	}



	// Check space on current line
	if (ssd1306_cursor_x + font.width > DISPLAY_WIDTH) {
		//Not enough space
		//Is wrapping enabled?
		if (wrap) {
		//Is there enough vertical space for new line?
			if (ssd1306_cursor_y + font.height > DISPLAY_HEIGHT) {
				// No:  break operation
				retval = 0;
			}
			else {
				// Yes: set cursor to start of next line
				ssd1306_cursor_x = 0;
				ssd1306_cursor_y += font.height;
			}
		}
		else {
			//No wrapping
			retval = 0;
		}
	}

	if (retval != 0) {
		// There is space, print the character

		for (j = 0; j < font.height; j++) {
			// TODO: Fix the following
			for (int k = 0; k < xblocks; k++) {
				byte = font.bitmap[(chr - 32) * (xblocks * font.height) + (xblocks*j) + k];
				if (k+1 <= xblocks ) {
					// Full byte
					for (i = 0; i < 8; i++) {
						uint8_t color = (byte & (1 << (7-i))) >> (7-i) ;
						ssd1306_setPixel(ssd1306_cursor_x + k*8 + i, ssd1306_cursor_y + 2*j+0, color);
						ssd1306_setPixel(ssd1306_cursor_x + k*8 + i, ssd1306_cursor_y + 2*j+1, color);
					}
				}
				else {
					// Leftover part
					for (i = 0; i < font.width % 8; i++) {
						uint8_t color = (byte & (1 << (7-i))) >> (7-i) ;
						ssd1306_setPixel(ssd1306_cursor_x + k*8 + i, ssd1306_cursor_y + 2*j+0, color);
						ssd1306_setPixel(ssd1306_cursor_x + k*8 + i, ssd1306_cursor_y + 2*j+1, color);
					}
				}
			}
		}
	}
	return retval;
}

void ssd1306_setCursor(uint32_t x, uint32_t y) {
	ssd1306_cursor_x = x;
	ssd1306_cursor_y = y;
}
