#ifndef FONTS_H
#define FONTS_H

#include <stdint.h>

typedef struct {
	uint8_t width;
	uint8_t height;
	const uint8_t *bitmap;
} FontType;


//extern FontType FiraCode_32;
extern FontType DejaVu_32;
extern FontType DejaVu_8;
#endif /* FONTS_H */
