/*
 * ssd1306.h
 *
 *  Created on: Mar 31, 2020
 *      Author: Kuosch
 */

#ifndef SSD1306_H_
#define SSD1306_H_

#include <stdbool.h>
#include "stm32f0xx_hal.h"
#include "fonts.h"

// Adjust to your HAL
#define SSD1306_I2C_PORT	hi2c1
#define SSD1306_I2C_ADDRESS 0x78

/*------------ Command codes ------------*/
// Fundamental commands
#define SET_CONTRAST		0x81
#define DISPLAY_ALL_RESUME	0xA4
#define DISPLAY_ALL_ON		0xA5
#define DISPLAY_NORMAL		0xA6
#define DISPLAY_INVERSE		0xA7
#define DISPLAY_OFF			0xAE
#define DISPLAY_ON			0xAF

// Scrolling commands
#define SCROLL_RIGHT		0x26
#define SCROLL_LEFT			0x27
#define SCROLL_VERT_RIGHT	0x29
#define SCROLL_VERT_LEFT	0x2A
#define SCROLL_STOP			0x2E
#define SCROLL_START		0x2F
#define SCROLL_VERT_AREA	0xA3

// Addressing commands
#define SET_COLUMN_LOW_START	0x00
#define SET_COLUMN_HIGH_START 	0x10
#define	SET_MEMORY_MODE		0x20
#define MEMORY_MODE_HORIZ	0x00
#define MEMORY_MODE_VERT	0x01
#define MEMORY_MODE_PAGE	0x02

#define	SET_COLUMN_ADDR		0x21
#define	SET_PAGE_ADDR		0x22
#define	SET_PAGE_START_OFFS	0xB0

// Hardware configuration commands
#define	SET_START_LINE		0x40
#define SEGMENT_REMAP		0xA0
#define MULTIPLEX_RATIO		0xA8
#define COM_SCAN_DIR_INC	0xC0
#define COM_SCAN_DIR_DEC	0xC8
#define DISPLAY_OFFSET		0xD3
#define COM_PINS			0xDA

// Timing and driving settings commands
#define SET_DISPLAY_CLOCK		0xD5
#define SET_PRE_CHARGE			0xD9

#define SET_VCOM_DESELECT_LVL	0xDB
#define VCOM_LEVEL_065_VCC		0x00
#define VCOM_LEVEL_077_VCC		0x20
#define VCOM_LEVEL_083_VCC		0x30

#define SSD1306_NOP			0xE3

// Other
#define CHARGE_PUMP			0x8D
#define ENABLE_CHARGE_PUMP	0x14
#define DISABLE_CHARGE_PUMP 0x10

#define PAGES		8

#define DISPLAY_HEIGHT	64
#define DISPLAY_WIDTH	128

#define I2C_TIMEOUT		10

#define ON	1
#define OFF 0
#define INV 2


/*------------ Variables ------------*/

static uint32_t ssd1306_cursor_x = 0;
static uint32_t ssd1306_cursor_y = 0;

/*------------ Function prototypes ------------*/

void ssd1306_init(void);
void ssd1306_sendCommand(uint8_t command);

void ssd1306_clearDisplay(void);
void ssd1306_invertDisplay(void);
void ssd1306_displayRefresh(void);
void ssd1306_setPixel(uint32_t, uint32_t, uint32_t);
void ssd1306_displayImage(uint8_t *image);

void ssd1306_scrollRight(void);
void ssd1306_scrollLeft(void);
void ssd1306_scrollVertRight(void);
void ssd1306_scrollVertLeft(void);
void ssd1306_scrollStart(void);
void ssd1306_scrollStop(void);

char ssd1306_printString(char*, FontType, uint32_t, uint32_t);
char ssd1306_printChar(char, FontType, uint32_t, bool);
void ssd1306_setCursor(uint32_t, uint32_t);

#endif /* SSD1306_H_ */
