A driver for ssd1306-based oled displays, controlled with STM32F0xx MCU.

*Currently only tested on 128 x 32 pixel displays.
*Fonts converted from DejaVu Sans Monospace free typeface.

NOTES: The display memory has 64 lines. On the tested 32 line display, every other line in memory is skipped.

TODO: Add remaining register definitions
TODO: Add other font sizes
